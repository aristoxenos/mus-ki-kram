import pandas as pd
import glob

files = glob.glob('*.csv')
files.sort()
files.pop(-1)
#files.remove('test_set_mean.csv')
#files.remove('test_set_std.csv')
print(files)
print('')

file1 = pd.read_csv('Track_1.csv', index_col = 0)

features = [
    'SpectralCentroid_mean', 'SpectralSpread_mean',
    'SpectralFlux_mean', 'Roughness_mean',
    'Sharpness_mean', 'SPL_mean', 'CorrelationDimension'
]

data_mean = pd.DataFrame([], index = file1.columns)
data_mean = data_mean.T
data_std = pd.DataFrame([], index = file1.columns)
data_std = data_std.T

for file in files:
    df = pd.read_csv(file, index_col = 0)
    data_mean = data_mean.append(df.mean('index'), ignore_index = True)
    data_std = data_std.append(df.std('index'), ignore_index = True)

print(data_mean)

data_mean.to_csv('test_set_mean.csv')
data_std.to_csv('test_set_std.csv')
